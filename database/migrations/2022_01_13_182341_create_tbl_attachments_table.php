<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_attachments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('brand_id');
            $table->uuid('model_id');
            $table->uuid('repair_type_id');
            $table->string('attachment_path');
            $table->string('attachment_name');
            $table->string('attachment_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_attachments');
    }
}
