<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('saturation')->nullable();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('street');
            $table->string('house_no');
            $table->string('post_code');
            $table->string('location')->nullable();
            $table->string('country');
            $table->string('email');
            $table->integer('total_charge');
            $table->date('date');
            $table->tinyInteger('agree_flag')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_customers');
    }
}
