<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_models', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('brand_id');
            $table->string('model_name')->nullable();
            $table->string('model_name_en')->nullable();
            $table->string('model_number')->nullable();
            $table->string('model_number_en')->nullable();
            $table->tinyInteger('status')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_models');
    }
}
