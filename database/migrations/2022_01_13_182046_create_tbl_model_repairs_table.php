<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblModelRepairsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_model_repairs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('model_id');
            $table->uuid('repair_type_id');
            $table->string('description')->nullable();
            $table->string('description_en')->nullable();
            $table->integer('price');
            $table->tinyInteger('status')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_model_repairs');
    }
}
