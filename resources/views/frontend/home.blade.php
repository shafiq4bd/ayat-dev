@extends('frontend.layouts.app')

@section('content')
                @php
                    $featured_categories = \App\Category::where('featured', 1)->get();
                @endphp
                
                    @if (get_setting('home_slider_images') != null)
                            <div class="aiz-carousel dots-inside-bottom mobile-img-auto-height" data-arrows="true" data-dots="true" data-autoplay="true">
                                @php $slider_images = json_decode(get_setting('home_slider_images'), true);  @endphp
                                @foreach ($slider_images as $key => $value)
                                    <div class="carousel-box">
                                        <a href="{{ json_decode(get_setting('home_slider_links'), true)[$key] }}">
                                            <img
                                                class="d-block mw-100 img-fit rounded shadow-sm overflow-hidden"
                                                src="{{ uploaded_asset($slider_images[$key]) }}"
                                                alt="{{ env('APP_NAME')}} promo"
                                                @if(count($featured_categories) == 0)
                                                height="500"
                                                @else
                                                height="500"
                                                @endif
                                                onerror="this.onerror=null;this.src='{{ static_asset('assets/img/placeholder-rect.jpg') }}';"
                                            >
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        @endif
    
            <div class="container">
               
            
                <div class="module-wrapper">
                    <div class="row">
                        <div class="col-lg-4">
                            <a href="{{ route('mobile-repair') }}">
                                <div class="module-area">
                                    <img class="lazyload module-img" src="{{ static_asset('assets/img/icon/support.png') }}"  alt="{{ env('APP_NAME') }}">
                                    <h4 class="module-title">Mobile Repair </h4>
                                
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4">
                            <a href="{{ route('repair-info') }}">
                                <div class="module-area">
                                    <img class="lazyload module-img" src="{{ static_asset('assets/img/icon/buy-button.png') }}"  alt="{{ env('APP_NAME') }}">
                                    <h4 class="module-title">Mobile Repair Information</h4>
                                
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4">
                            <a href="{{ route('mobile-buy') }}">
                                <div class="module-area">
                                    <img class="lazyload module-img" src="{{ static_asset('assets/img/icon/phone-case.png') }}"  alt="{{ env('APP_NAME') }}">
                                    <h4 class="module-title">Mobile Buy and Sell</h4>
                                
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>


@endsection

@section('script')

@endsection
