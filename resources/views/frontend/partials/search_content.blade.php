
<div class="">
    @if (count($models) > 0)
        <div class="px-2 py-1 text-uppercase fs-10 text-right text-muted bg-soft-secondary">{{translate('Mobile Model')}}</div>
        <ul class="list-group list-group-raw">
            @foreach ($models as $key => $model)
                <li class="list-group-item search-item">
                    <a class="text-reset" href="{{route('repair-type')}}">
                        <div class="d-flex search-product align-items-center">
                           
                            <div class="flex-grow-1 overflow--hidden minw-0">
                                <div class="product-name text-truncate fs-16 font-weight-bold mb-5px text-center">
                                    {{  $model->model_name  }}
                                </div>
                            </div>
                        </div>
                    </a>
                </li>
            @endforeach
        </ul>
    @endif
</div>
