@extends('frontend.layouts.app')

@section('content')

    <div class="container">
        <div class="report-wrap">
            <h4 class="report-no">No. 1579174238</h4>
            <div class="alert alert-primary text-center" role="alert">
                Your order has been successfully received by us.You can track the status
                of your order on this page.
            </div>
            <div class="brand-area report-brand-area">
                <div class="row d-flex align-items-center justify-content-center">
                    <div class="col-lg-4 ">
                        <div class="brand-wrap">
                            <div class="item">
                                <img class="lazyload module-img" src="{{ static_asset('assets/img/brand.png') }}"  alt="{{ env('APP_NAME') }}">
                                <p>Samsung</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 ">
                        <div class="brand-wrap">
                            <div class="item">
                                <img class="lazyload module-img" src="{{ static_asset('assets/img/model.jpg') }}"  alt="{{ env('APP_NAME') }}">
                                <p>Samsung s9</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="report-area">
                    <div class="report-wrapper">
                        <div class="label-area">
                            <p>Status </p>
                            <p class="items">:</p>
                        </div>
                        <label>&nbsp;Open minded</label>
                    </div>
                    <div class="report-wrapper">
                        <div class="label-area">
                            <p>Date </p>
                            <p class="items">:</p>
                        </div>
                        <label>&nbsp;1/15/2022 6:23 p.m</label>
                    </div>
                    <div class="report-wrapper">
                        <div class="label-area">
                            <p>Device </p>
                            <p class="items">:</p>
                        </div>
                        <label>&nbsp;Apple iPhone XS</label>
                    </div>
                    <div class="report-wrapper">
                        <div class="label-area">
                            <p>Repairs </p>
                            <p class="items">:</p>
                        </div>
                        <label>&nbsp;Error analysis & cost estimate</label>
                    </div>
                    <div class="report-wrapper">
                        <div class="label-area">
                            <p>Estimated cost </p>
                            <p class="items">:</p>
                        </div>
                        <label>&nbsp;€ 20.00 (incl. VAT)</label>
                    </div>
                    <div class="report-wrapper">
                        <div class="label-area">
                            <p>Completion </p>
                            <p class="items">:</p>
                        </div>
                        <label>&nbsp;Shipping (Free round-trip shipping)</label>
                    </div>
                    <div class="report-wrapper">
                        <div class="label-area">
                            <p>Payment method </p>
                            <p class="items">:</p>
                        </div>
                        <label>&nbsp;Online Payment(Not Paid)</label>
                    </div>
                    <div class="report-wrapper">
                        <div class="label-area">
                            <p>Error Description</p>
                            <p class="items">:</p>
                        </div>
                        <label>&nbsp;sdkj sodfjlsdfj lskdfjl kjlksajdf lkjm</label>
                    </div>

                </div>
                <div class="button-area">
                    <a class="btn btn-success" href="#"> Download Report</a>
                </div>

            </div>


        </div>

    </div>
@endsection

@section('script')

@endsection
