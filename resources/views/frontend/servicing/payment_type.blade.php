@extends('frontend.layouts.app')

@section('content')
    <div class="container">
        <div class="brand-area">
            <h4 class="toptitle">Brand/Model</h4>

            <div class="row d-flex align-items-center justify-content-center">
                <div class="col-lg-4 ">
                    <div class="brand-wrap">
                        <div class="item">
                            <img class="lazyload module-img" src="{{ static_asset('assets/img/brand.png') }}"  alt="{{ env('APP_NAME') }}">
                            <p>Samsung</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 ">
                    <div class="brand-wrap">
                        <div class="item">
                            <img class="lazyload module-img" src="{{ static_asset('assets/img/model.jpg') }}"  alt="{{ env('APP_NAME') }}">
                            <p>Samsung s9</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="repair-type-wrap">
            <h4 class="toptitle">Repair Types</h4>
            <ul class="list-group repair-item-wrap">
                <li class="single-item">
                    <div class="d-flex search-product align-items-center">

                    <img class="lazyload repair-icon" src="{{ static_asset('assets/img/p1.png') }}"  alt="{{ env('APP_NAME') }}">
                    <p class="product-name text-truncate mb-5px">
                            Display repair
                    </p>
                    </div>
                    <p>€ 39.00</p>
                </li>
                <li class="single-item">
                    <div class="d-flex search-product align-items-center">
                    <img class="lazyload repair-icon" src="{{ static_asset('assets/img/p1.png') }}"  alt="{{ env('APP_NAME') }}">
                    <p class="product-name text-truncate mb-5px">
                            Display repair & Servicing
                    </p>
                    </div>
                    <p>€ 100.00</p>
                </li>
                <li class="single-item">
                    <div class="d-flex search-product align-items-center">

                    <img class="lazyload repair-icon" src="{{ static_asset('assets/img/p2.png') }}"  alt="{{ env('APP_NAME') }}">
                    <p class="product-name text-truncate mb-5px">
                            Battery repair
                    </p>
                    </div>
                    <p>€ 200.00</p>
                </li>
            </ul>
        </div>

        <div class="shipping-wrap">
            <h4 class="toptitle">How would you like to proceed?</h4>

            <div class="shipping-area">
                <div class="radio-wrapper">
                    <div class="form-check-radio">
                        <input class="radio-input" type="radio" name="free_shipping" id="free_shipping" value="option1" checked>
                        <label for="free_shipping">
                        Shipping (Free round-trip shipping)
                        </label>
                    </div>
                    <p>Send us your device well packed. We will take care of it as soon as it arrives.</p>
                </div>
                <div class="radio-wrapper">
                    <div class="form-check-radio">
                        <input class="radio-input" type="radio" name="free_shipping" id="free_shipping" value="option1" checked>
                        <label for="free_shipping">
                            Bring directly to our branch
                        </label>
                    </div>
                    <p>IMMEDIATE REPAIRS POSSIBLE Mon-Fri 9:00 a.m. - 6:00 p.m</p>
                </div>

                <div class="radio-wrapper">
                    <div class="form-check-radio">
                        <input class="radio-input" type="radio" name="free_shipping" id="free_shipping" value="option1" checked>
                        <label for="free_shipping">
                            Repair offer for multiple damages/devices
                        </label>
                    </div>
                    <p>Please describe your concern in the error description field and complete the request. You will receive a repair
                        offer with special combination prices from
                        us immediately and free of charge. You are also welcome to call our hotline directly on 01/9611727.</p>
                </div>

            </div>
        </div>

    <div class="next-area">
        <h4 class="toptitle">
            What's next?
        </h4>
        <p class="next-desc">Select here how you want to pay AFTER the repair has been completed. Only after a successful repair will you receive an invoice that you can pay online.</p>
        <div class="radio-wrapper">
            <div class="form-check-radio">
                <input class="radio-input" type="radio" name="online" id="online" value="option1" checked>
                <label for="online">
                    online payment
                </label>
            </div>
        </div>
        <div class="radio-wrapper">
            <div class="form-check-radio">
                <input class="radio-input" type="radio" name="transfer" id="transfer" value="option1" checked>
                <label for="transfer">
                    transfer
                </label>
            </div>
        </div>
        <div class="radio-wrapper">
            <div class="form-check-radio">
                <input class="radio-input" type="radio" name="cash_on" id="cash_on" value="option1" checked>
                <label for="cash_on">
                    Cash on delivery
                </label>
            </div>
        </div>
        <div class="button-area">
            <a class="btn btn-info" href="{{route('repair-type')}}"> Back</a>
            <a class="btn btn-success" href="{{route('customer-register')}}"> Choose</a>
        </div>
    </div>


    </div>
@endsection

@section('script')

@endsection
