@extends('frontend.layouts.app')

@section('content')
    <div class="container">
        <div class="brand-area">
            <h4 class="toptitle">Brand/Model</h4>

            <div class="row d-flex align-items-center justify-content-center">
                <div class="col-lg-4 ">
                    <div class="brand-wrap">
                        <div class="item">
                            <img class="lazyload module-img" src="{{ static_asset('assets/img/brand.png') }}"  alt="{{ env('APP_NAME') }}">
                            <p>Samsung</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 ">
                    <div class="brand-wrap">
                        <div class="item">
                            <img class="lazyload module-img" src="{{ static_asset('assets/img/model.jpg') }}"  alt="{{ env('APP_NAME') }}">
                            <p>Samsung s9</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="repair-type-wrap">
            <h4 class="toptitle">Repair Types</h4>
            <ul class="list-group repair-item-wrap">
                <li class="single-item">
                    <div class="d-flex search-product align-items-center">

                    <img class="lazyload repair-icon" src="{{ static_asset('assets/img/p1.png') }}"  alt="{{ env('APP_NAME') }}">
                    <p class="product-name text-truncate mb-5px">
                            Display repair
                    </p>
                    </div>
                    <p>€ 39.00</p>
                </li>
                <li class="single-item">
                    <div class="d-flex search-product align-items-center">
                    <img class="lazyload repair-icon" src="{{ static_asset('assets/img/p1.png') }}"  alt="{{ env('APP_NAME') }}">
                    <p class="product-name text-truncate mb-5px">
                            Display repair & Servicing
                    </p>
                    </div>
                    <p>€ 100.00</p>
                </li>
                <li class="single-item">
                    <div class="d-flex search-product align-items-center">

                    <img class="lazyload repair-icon" src="{{ static_asset('assets/img/p2.png') }}"  alt="{{ env('APP_NAME') }}">
                    <p class="product-name text-truncate mb-5px">
                            Battery repair
                    </p>
                    </div>
                    <p>€ 200.00</p>
                </li>
            </ul>
        </div>

        <div class="shipping-wrap">
            <h4 class="toptitle">Shipment & Payment</h4>
            <div class="shipment-payment-wrap">
                <h4 class="items">Shipping (Free round-trip shipping)</h4>
                <h4 class="items">Online Payment</h4>
            </div>
        </div>

        <div class="shipping-wrap personal-wrap">
            <h4 class="toptitle">Personal Data</h4>
            <form class="register-area">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="salutation">Salutation</label>
                            <input type="text" class="form-control"name="salutation">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="first_name">First Name</label>
                            <input type="text" class="form-control" name="first_name">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="street">Street</label>
                            <input type="text" class="form-control"  name="street">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="house_no">House No</label>
                            <input type="text" class="form-control"  name="house_no">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="post_code">Post Code</label>
                            <input type="text" class="form-control"  name="post_code">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="location">Location</label>
                            <input type="text" class="form-control"  name="location">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="country">Country</label>
                            <input type="text" class="form-control"  name="country">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="email">Email address</label>
                            <input type="email" class="form-control"  name="email">
                        </div>
                    </div>
                </div>

                <div class="form-group form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Please confirm that you agree to the electronic processing of your data in
                        accordance with our data protection guidelines and our terms and conditions .</label>
                </div>
                <div class="button-area">
                    <a class="btn btn-info" href="{{route('payment-type')}}"> Back</a>
                    <a class="btn btn-success" href="{{route('report')}}"> Complete Order</a>
                </div>
            </form>
        </div>

    </div>
@endsection

@section('script')

@endsection
