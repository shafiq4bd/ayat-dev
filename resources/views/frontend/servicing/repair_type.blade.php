@extends('frontend.layouts.app')

@section('content')

    <div class="container">
        <div class="brand-area">
            <h4 class="toptitle">Brand/Model</h4>
            <div class="row d-flex align-items-center justify-content-center">
                <div class="col-lg-4 ">
                    <div class="brand-wrap">
                        <div class="item">
                            <img class="lazyload module-img" src="{{ static_asset('assets/img/brand.png') }}"  alt="{{ env('APP_NAME') }}">
                            <p>Samsung</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 ">
                    <div class="brand-wrap">
                        <div class="item">
                            <img class="lazyload module-img" src="{{ static_asset('assets/img/model.jpg') }}"  alt="{{ env('APP_NAME') }}">
                            <p>Samsung s9</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="repair-type-area">
            <h4 class="toptitle">Repair Types</h4>
            <!-- Start Module Section-->
            <section class="module-section section-padding">
                <div class="sidebar-products-main">
                    <div id="accordion">
                        <div class="card">
                            <div class="sidebar-single">
                                <div class="sidebar-title">
                                    <a data-toggle="collapse" class="d-flex align-items-center " data-target="#Sarker1" aria-expanded="false"  aria-controls="#Sarker1">
                                        <img class="lazyload repair-icon" src="{{ static_asset('assets/img/p1.png') }}"  alt="{{ env('APP_NAME') }}">
                                        <span class="pull-left title-sidebar">Display repair </span>
                                        <span class="pull-right"><i class="fa fa-plus"></i></span>
                                        <span class="pull-right"><i class="fa fa-minus"></i></span>
                                        <div class="clearfix"></div>
                                    </a>
                                    <div class="price-area">
                                        <p>€ 65.00</p>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                            <label class="form-check-label" for="defaultCheck1">
                                            </label>
                                        </div>
                                    </div>

                                </div>

                                <div id="Sarker1" class="collapse" data-parent="#accordion">
                                    <div class="collapse-content">
                                        In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to
                                        demonstrate the visual form of a document or a typeface without relying on meaningful content
                                    </div>
                                </div>
                            </div>

                            <div class="sidebar-single">
                                <div class="sidebar-title">
                                    <a data-toggle="collapse" class="d-flex align-items-center " data-target="#Sarker2" aria-expanded="false"  aria-controls="#Sarker2">
                                        <img class="lazyload repair-icon" src="{{ static_asset('assets/img/p2.png') }}"  alt="{{ env('APP_NAME') }}">
                                        <span class="pull-left title-sidebar">Battery repair </span>
                                        <span class="pull-right"><i class="fa fa-plus"></i></span>
                                        <span class="pull-right"><i class="fa fa-minus"></i></span>
                                        <div class="clearfix"></div>
                                    </a>
                                    <div class="price-area">
                                        <p>€ 200.00</p>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                            <label class="form-check-label" for="defaultCheck1">
                                            </label>
                                        </div>
                                    </div>

                                </div>

                                <div id="Sarker2" class="collapse" data-parent="#accordion">
                                    <div class="collapse-content">
                                        In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to
                                        demonstrate the visual form of a document or a typeface without relying on meaningful content
                                    </div>
                                </div>
                            </div>
                            <div class="sidebar-single">
                                <div class="sidebar-title">
                                    <a data-toggle="collapse" class="d-flex align-items-center " data-target="#Sarker3" aria-expanded="false"  aria-controls="#Sarker3">
                                        <img class="lazyload repair-icon" src="{{ static_asset('assets/img/p1.png') }}"  alt="{{ env('APP_NAME') }}">
                                        <span class="pull-left title-sidebar">Display Change and repair</span>
                                        <span class="pull-right"><i class="fa fa-plus"></i></span>
                                        <span class="pull-right"><i class="fa fa-minus"></i></span>
                                        <div class="clearfix"></div>
                                    </a>
                                    <div class="price-area">
                                        <p>€ 100.00</p>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                            <label class="form-check-label" for="defaultCheck1">
                                            </label>
                                        </div>
                                    </div>

                                </div>

                                <div id="Sarker3" class="collapse" data-parent="#accordion">
                                    <div class="collapse-content">
                                        In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to
                                        demonstrate the visual form of a document or a typeface without relying on meaningful content
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="total-price">
                        <p>Estimated total cost: <span>€199.00</span></p>
                    </div>
                    <div class="button-area">
                        <a class="btn btn-info" href="{{route('mobile-repair')}}"> Back</a>
                        <a class="btn btn-success" href="{{route('payment-type')}}"> Choose</a>
                    </div>
                </div>
            </section>
        </div>

    </div>
@endsection

@section('script')

@endsection
