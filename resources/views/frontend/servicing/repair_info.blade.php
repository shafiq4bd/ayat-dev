@extends('frontend.layouts.app')

@section('content')
    
    <div class="container">
        <div class="repair-wrap">
            <div class="row">
                <div class="col-lg-12">
                    <div class="repair-area">
                        <img class="lazyload repair-img" src="{{ static_asset('assets/img/repair-img.png') }}"  alt="{{ env('APP_NAME') }}">
                        
                    </div>
                </div>
            </div>
            <div class="repair-content">
                <h4>iPhone / cell phone repair</h4>
                <p>Is your <b> touchscreen broken or not working ?</b></p>
                <p>Has <b> the mobile phone / tablet fallen out of </b> your hand and you can no longer see anything on the display?</p>
                <p>Has your <b> device </b> suddenly just given up <b> the "ghost" ?</b></p>
                <br>
                <p>Are you looking for a <b> mobile phone repair service in Vienna ? </b></p>
                <p>Then you are right with us!</p>
                <p>We would be happy to take a look at your device and find the <b> best solution </b> for you and your wallet. <br>
                The company <b> Mobile Phone Smart Vienna repaired </b> the various damages that may arise on mobile phones.<br>
                It is not for nothing that many external repair workshops as well as private customers turn to us for tricky errors in order to have the device repaired in our workshop. <b> We offer you a service for many manufacturers!</b>
                The spare parts required for this are in stock for most repairs.</p>

                <h4>We offer you the following mobile phone repairs:</h4>
                <ul>
                    <li> Battery replacement</li>
                    <li> Battery cover / backplate exchange</li>
                    <li> Unlock without loss of warranty</li>
                    <li>All kinds of software solutions</li>
                    <li> Reset the Google account</li>
                    <li> LCD / display replacement</li>
                    <li>Exchange of various ICs on the boards </li>
                    <li> Replacement of the charging socket</li>
                    <li> Microphone-speaker-receiver exchange</li>
                    <li> Exchange of various flex cables</li>
                    <li>various soldering work </li>
                    <li> Housing replacement</li>
                    <li>Midframe exchange </li>
                    <li> Sensor flex exchange</li>
                    <li>Volume key exchange </li>
                    <li> Main and front camera swap</li>
                    <li> Exchange of the SIM reader</li>
                    <li> ... and much more ... Do not hesitate and ask us, we will be happy to help you.</li>
                </ul>
                <div class="buy-link">
                    <a href="{{ route('mobile-buy') }}">
                        <img class="lazyload buy-img" src="{{ static_asset('assets/img/buy.png') }}"  alt="{{ env('APP_NAME') }}">
                        
                    </a>
                </div>

               
            </div>
        
        </div>  
    </div>


@endsection

@section('script')

@endsection
