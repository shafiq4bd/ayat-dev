@extends('frontend.layouts.app')

@section('content')
    
    <div class="container">
        <div class="search-area">
            <div class="flex-grow-1 front-header-search  search-wrapper mt-10">
            <h1 class="search-title">Your repair shop for smartphones, tablets & co</h1>
                <div class="position-relative flex-grow-1 cust-group">
                    <form action="{{ route('search') }}" method="GET" class="stop-propagation">
                        <div class="">
                            <div class="d-lg-none" data-toggle="class-toggle" data-target=".front-header-search">
                                <button class="btn px-2" type="button"><i class="la la-2x la-long-arrow-left"></i></button>
                            </div>
                            
                            <div class="input-group ">
                            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                <input type="text" class="border-0 search-input border-lg form-control" id="search" name="q" placeholder="{{translate('Search a model')}}" autocomplete="off">
                                <div class="input-group-append  d-none d-lg-block">
                                    <button class="btn btn-primary search-btn" type="submit">
                                        <i class="la la-search la-flip-horizontal fs-18"></i>
                                    </button> 
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="typed-search-box stop-propagation document-click-d-none d-none bg-white rounded shadow-lg position-absolute left-0 top-100 w-100" style="min-height: 200px">
                        <div class="search-preloader absolute-top-center">
                            <div class="dot-loader"><div></div><div></div><div></div></div>
                        </div>
                        <div class="search-nothing d-none p-3 text-center fs-16">

                        </div>
                        <div id="search-content" class="text-left">

                        </div>
                    </div>
                </div>
            </div>

        </div>
        
    </div>  


@endsection

@section('script')

@endsection
