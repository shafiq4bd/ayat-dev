<?php

namespace App\Http\Controllers;

use DB;
use Session;
use Auth;
use Hash;
use App\Category;
use App\FlashDeal;
use App\Brand;
use App\Product;
use App\PickupPoint;
use App\ModelTbl;
use App\CustomerPackage;
use App\CustomerProduct;
use App\User;
use App\Seller;
use App\Shop;
use App\Color;
use App\Order;
use App\BusinessSetting;
use App\Http\Controllers\SearchController;
use ImageOptimizer;
use Cookie;
use Illuminate\Support\Str;
use App\Mail\SecondEmailVerifyMailManager;
use Mail;
use App\Utility\TranslationUtility;
use App\Utility\CategoryUtility;
use Illuminate\Auth\Events\PasswordReset;



use Illuminate\Http\Request;

class ServicingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function home()
    {
        return view('frontend.home');
    }

    public function mobile_repair()
    {
        return view('frontend.mobile_repair');
    }
    public function repair_info()
    {
        return view('frontend.servicing.repair_info');
    }

    public function mobile_buy()
    {
        return view('frontend.index');
    }

    public function repair_type(Request $request)
    {
//        $detailedProduct  = Product::where('publish', 1)->first();
//
//        if($detailedProduct != null){
//            return view('frontend.product_details', compact('detailedProduct'));
//            }else{
//
//                abort(404);
//            }
        return view("frontend.servicing.repair_type");
    }


    public function payment_type()
    {
        return view("frontend.servicing.payment_type");
    }

    public function ajax_search(Request $request)
    {
        $keywords = array();
        $models = DB::table('tbl_models')->where('status', 1)->where('model_name', 'like', '%'.$request->search.'%')->get();

        if(sizeof($models)>0){
            return view('frontend.partials.search_content', compact('models'));
        }
        return '0';
    }
    public function customer_register(){
        return view("frontend.servicing.register");
    }

    public function report(){
        return view("frontend.servicing.report");
    }


}
